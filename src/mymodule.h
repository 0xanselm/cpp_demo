#include <algorithm>
#include <any>
#include <array>
#include <cmath>
#include <iostream>
#include <memory>
#include <string>
#include <tuple>
#include <vector>
#ifndef MYMODULE_H
#define MYMODULE_H

namespace shapes {

struct Data {
    static const int SIZE = 100;
};

class Shape {
   protected:
    int mA, mB = 0;
    // to do: copy ctor, move sem
   public:
    Shape(int a, int b = 0);
    virtual ~Shape();
    virtual std::vector<std::any> getMembers() const = 0;
    virtual void setMembers(const std::vector<std::any>& args) = 0;
    virtual float getArea() = 0;
};

class Rectangle : public Shape {
   public:
    Rectangle(int a, int b);
    ~Rectangle() override;
    std::vector<std::any> getMembers() const override;
    void setMembers(const std::vector<std::any>& args) override;
    float getArea() override;
};

}  // namespace shapes

#endif