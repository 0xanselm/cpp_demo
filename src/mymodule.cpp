#include "mymodule.h"

namespace shapes {

Shape::Shape(int a, int b) : mA(a), mB(b) {
    std::cout << "Shape::Constructor\n";
}

Shape::~Shape() {
    std::cout << "Shape::Destructor\n";
}

Rectangle::Rectangle(int a, int b) : Shape(a, b) {
    std::cout << "Rectangle::Constructor\n";
}

Rectangle::~Rectangle() {
    std::cout << "Rectangle::Destructor\n";
}

std::vector<std::any> Rectangle::getMembers() const {
    std::vector<std::any> members;
    members.push_back(this->mA);
    members.push_back(this->mB);
    std::cout << "Members: ";
    for (const auto& i : members) {
        if (i.type() == typeid(int)) {
            std::cout << std::any_cast<int>(i) << " ";
        }
    }
    std::cout << std::endl;
    return members;
}

void Rectangle::setMembers(const std::vector<std::any>& args) {
    this->mA = std::any_cast<int>(args[0]);
    this->mB = std::any_cast<int>(args[1]);
}

float Rectangle::getArea() {
    std::cout << "Area Rectangle:" << this->mA * this->mB << std::endl;
    return this->mA * this->mB;
}
}  // namespace shapes