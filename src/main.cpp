#ifndef ENABLE_TESTS
#include "mymodule.h"

void myF(const int&& a) {
    switch (a) {
        case 1:
            std::cout << "Case a\n";
    }
};

int main() {
    std::cout << "My Module\n";
    myF(4);
    exit(0);
    std::unique_ptr<shapes::Shape> r(new shapes::Rectangle(3, 4));
    shapes::Rectangle r2(1, 2);
    r->getMembers();
    r->setMembers({5, 9});
    r->getMembers();
    r->getArea();
    return 0;
}
#endif

#ifdef ENABLE_TESTS
#include "mymodule_test.h"
int main(int argc, char** argv) {
    std::cout << "My Tests\n";
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
#endif