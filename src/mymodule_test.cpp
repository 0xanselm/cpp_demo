// mymodule_test.cpp

#include "mymodule_test.h"

TEST(MyModuleTest, TestFunction1) {
    std::unique_ptr<shapes::Shape> r(new shapes::Rectangle(3, 4));
    std::vector<int> expected{3, 4};
    std::vector<std::any> actual = r->getMembers();
    std::vector<int> actualInt;
    for (const auto& i : actual) {
        actualInt.push_back(std::any_cast<int>(i));
    }
    EXPECT_EQ(expected, actualInt);
}