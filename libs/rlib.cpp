#include "rlib.h"

template <typename T>
T square(T num) {
    return num * num;
}

template int square<int>(int num);
template float square<float>(float num);
template double square<double>(double num);