#include <iostream>

#include "rlib.h"

int main() {
    std::cout << "My shared lib test\n";
    std::cout << square(1) << std::endl;
    std::cout << square(2.0f) << std::endl;  // float - 7bit mantissa
    std::cout << square(3.0) << std::endl;   // double - 15bit mantissa
    return 0;
}

/*
We need to compile our library source code into position-independent code (PIC):
    g++ -c -Wall -Werror -fpic rshlib.cpp -o rshlib.o
Now we need to actually turn this object file into a shared library. We will call it libfoo.so:
    g++ -shared -o librsh.so rshlib.o
Telling G++ where to find the shared library with -L
    g++ -L. -Wall -o test test.cpp -lrsh
Making the library available at runtime and telling the loader where to look for it (either via using LD_LIBRARY_PATH or using rpath)
    g++ -L. -Wl,-rpath=/home/r/Documents/c++_demo/libs -Wall -o test test.cpp -lrsh
*/