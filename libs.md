# Shared lib

Given files: `rlib.cpp` becomes `librsh.so` and `test.cpp` which utilizes the shared lib

1. We need to compile our library source code into position-independent code (PIC):
   `g++ -c -Wall -Werror -fpic rshlib.cpp -o rshlib.o`
2. Now we need to actually turn this object file into a shared library. We will call it rshlib.so:
   `g++ -shared -o librsh.so rshlib.o`
3. Telling G++ where to find the shared library with -L
   `g++ -L. -Wall -o test test.cpp -lrsh`
4. Making the library available at runtime and telling the linker where to look for it (either via using LD_LIBRARY_PATH or using rpath)
   `g++ -L. -Wl,-rpath=/home/r/Documents/c++\_demo/libs -Wall -o test test.cpp -lrsh`

# Static lib

Given files: `rlib.cpp` becomes `librst.a` and `test.cpp` which utilizes the shared lib

1. We need to compile the source file into an object file:
   `g++ -c rlib.cpp -o rstlib.o`
2. We create a static library using the object file:
   `ar rcs librst.a rstlib.o`
   - `ar`: create and manipulate static libraries Linux systems
   - `r`: replace or add files to the archive
   - `c`: create the archive if it doesn't exist
   - `s`: (Optional) write index into the archive. Recommended for faster symbol lookup
3. Making the library available and tell the loader where to look for it
   `g++ libsPOC.cpp -L. -lrst -o exe`
