---
markmap:
  maxWidth: 500
  colorFreezeLevel: 2
  pan: true
---

# c++ 17

## libraries

- boost

## objects

- In C++, an object is a region of storage that has a defined type and a value. The type of an object determines the operations that can be performed on it, while the value of an object is the data that it stores.

- An object can be a variable, a data member of a class or struct, an element of an array, or a dynamically allocated block of memory. When an object is created, space is allocated in memory to hold its value. The value of an object can be modified by assigning a new value to it, or by calling functions or methods that modify its value.

- Objects in C++ have various properties, including
  - Size: the number of bytes of memory that are required to store the - object's value
  - Lifetime: the period of time during which the object exists in memory
  - Storage duration: automatic, static, dynamic, thread-local
  - Scope: the region of code in which the object is visible and can be accessed.
  - Accessibility: the level of access that other parts of the program have to the object
  - Alignment requirement: can be determined with alignof
  - Type
  - Value: which may be indeterminate, e.g. for default-initialized non-class types
  - Name (optionally)
- Understanding objects is essential to writing correct and efficient C++ code. By using objects to represent data and algorithms in your program, you can create code that is more readable, more maintainable, and less error-prone

## types

- Fundamental types, such as `int`, `float`, and `bool`
- Pointer types, which represent memory addresses, such as `int*` and `char*`
- Reference types, which represent references to other objects, such as `int&` and `char&`
- Array types, which represent collections of objects of the same type, such as `int[]` and `char[]`.
- Function types, such as `int(int a)`
- In addition to the built-in types, C++ also allows you to define your own custom types using structures, classes, and enumerations.

## ownership

- ownership refers to the responsibility for managing the lifetime of an object. The owner of the memory block is the object that manages the lifetime of the smart pointer or container.

  - RAII (Resource Acquisition Is Initialization): RAII is a programming technique that involves acquiring resources (such as memory) in a constructor and releasing them in a destructor. By using RAII, you can ensure that resources are always released in a timely and consistent manner, even in the presence of exceptions or other error conditions.

  - Smart pointers: Smart pointers are objects that act like pointers, but also manage the memory that they point to. C++ provides several types of smart pointers, including `unique_ptr`, `shared_ptr`, and `weak_ptr`. Smart pointers can help manage ownership by automatically deallocating memory when it is no longer needed, either when the smart pointer goes out of scope or when the last reference to the memory is removed.

  - Move semantics: Move semantics are a C++11 feature that allows objects to be efficiently moved from one location to another, rather than being copied. This can be particularly useful for managing ownership, because it allows resources to be transferred from one object to another, rather than being copied and potentially duplicated.

## statements

- expressions
  - sequences of operators and operands that are used for one or more of these purposes
    - computing a value from the operands
    - designating objects or functions. a + b, the variables a and b are designated as the operands of the addition operator +
    - generating "side effects.", which are any actions other than the evaluation of the expression — for example, modifying the value of an object
    - for example, "2 + 3" is an expression that evaluates to the value 5.
- assignments
  - assigns a value to an already declared object, a variable or other storage location. A typically consists of a variable or storage location, an = to indicate assignment, and an expression that evaluates to the value to be assigned. "x = 5" is an assignment statement that assigns the value 5 to the variable x. in C++, a declaration can also include an initialization, which assigns an initial value to the declared object
- control flow statements
  - used to control the order in which statements are executed in a program. Examples include conditional statements (if/else, switch), loops (for, while, do-while), and jump statements (break, continue, goto, return).
- declarations
  - introduces a name and its associated type to the compiler. E.g. `int foo();`, `std::string a;`, `class MyClass;`. In C++, a declaration can also include an initialization, which assigns an initial value to the declared object
- function calls
  - call a function and pass arguments to it
- exceptions
  - exception statements are used to handle errors and other exceptional conditions in a program

## expressions

- casting operators
  - dynamic_cast Used for conversion of polymorphic types.
  - static_cast Used for conversion of nonpolymorphic types.
  - const_cast Used to remove the const, volatile, and \_\_unaligned attributes.
  - reinterpret_cast Used for simple reinterpretation of bits.

## keywords

- used to define the basic structure and syntax of a program, while qualifiers
- reserved words in the C++ language with special meanings. They are used to define statements, declarations, and other language constructs. Examples of keywords in C++ include int, void, class, if, else, and return.

## qualifiers

- used to provide additional information about the data types and objects used in the program. Behavior is determined by the context in which they are used. Examples of qualifiers in C++ include const, volatile, mutable, static, and extern.
- const

  - used to modify types and indicate additional information about variables and functions
  - initialization of a const variable can be deferred until run time
    - const int x = 5; // x is a constant integer
    - const int\* p = &x; // p is a pointer to a constant integer
    - void myFunc() const; // myFunc is a const member function and cannot modify the data members of the class
    - void f1(const int\* i)
      - the const qualifier applies to the int object being pointed to, indicating that the pointed-to object is not allowed to be modified through the pointer i. The pointer itself, however, can be reassigned to point to a different object.
    - void f2(int\* const i)
      - the const qualifier applies to the pointer i, indicating that the pointer itself is not allowed to be modified, i.e., i always points to the same memory location. The object being pointed to can be modified through the pointer.

- constexpr
  - constexpr variable must be initialized at compile time.
- extern
  - symbol has external linkage
- explicit
  - constructor can only be called explicitly with the exact argument type specified in the constructor declaration. Prefixing the explicit keyword to the constructor prevents the compiler from using that constructor for implicit conversions. Prevents subtle bugs that can arise from unexpected implicit conversions

## value category

- lvalue
  - memory location that has a name and can be assigned a value. Can appear on both sides of an assignment
- rvalue
  - temporary value that does not have a name or a memory location. Used to move the contents of an object to another object, rather than making a copy. Can only appear on the right side of an assignment
  - xvalue
    - "eXpiring" value, which means that the value is about to expire or go out of scope

## vocabulary

- free store
  - heap, dynamic store
- identifier
  - int age = 30; "age" is an identifier. sequence of characters that are used to identify a specific entity
  - keywords
    - reserved identifiers that have special meanings
- entity
  - a more general term that includes objects, but can also refer to other named program elements like variables, functions, and classes.
- object
  - concrete instance of a data type
- access specifiers
  - private
    - can only be accessed by member functions of the class itself
  - protected
    - can be accessed by member functions of the class itself and its derived classes
  - public
    - can be accessed by any code that can see the class
- lambda

  - auto identifier = [capture-list] (parameter-list) -> return-type{function-body}; 
    int a = 2;
    float b = 4.0f;
    auto z = [&a, &b]() -> float { return a \* b; };
    std::cout << "z: " << z();
  - There are two ways to capture variables: by value and by reference.
    [=] captures all variables from the enclosing scope by value.
    [&] captures all variables from the enclosing scope by reference.
    [var] captures the variable var by value.
    [&var] captures the variable var by reference.

- polymorphism
  - achieved through the use of virtual functions and inheritance
- non-polymorphic
  - types include simple data types like int, float, and double, as well as custom user-defined types that don't contain virtual functions
- prototype
  - just another name for a declaration of a function

## to do

- move semantics
- conan
- cast
- types
- translation units
- virtual functions (vtable)
- dynamic dispatch
- modul test, assertg

# linux

## ELF

- ELF (Executable and Linkable Format) is a common file format used for executable programs, object code, shared libraries, and core dumps on Unix and Unix-like systems, including Linux. When a C++ program is compiled and linked, the resulting executable file is typically in the ELF format. The ELF format is used to store various types of information about the program, such as its code and data sections, symbol tables, and relocation information. When the program is loaded into memory and executed, the operating system uses this information to set up the program's memory space, resolve external symbols, and perform other initialization tasks. In the case of a C++ program, the ELF file contains not only the machine code generated by the compiler, but also information about the program's static and dynamic libraries, which are linked at compile time and runtime, respectively. This information is used by the operating system's dynamic linker to resolve external symbols and load the necessary libraries into memory when the program is run. In summary, the connection between C++ code and ELF is that the ELF format is used to store executable programs and other related files on Unix and Unix-like systems, including Linux. When a C++ program is compiled and linked, the resulting executable file is typically in the ELF format.
  - Linking
    - is the process of combining object files generated by the compiler into a single executable program or library. When a C++ program is compiled, it is first translated into machine code and data in the form of object files. These object files contain the program's code and data in a format that the linker can understand. The linker takes these object files and combines them into a single executable program or library by resolving external references between them. This involves identifying symbols that are defined in one object file and referenced in another, and linking them together to form a complete program. There are two types of linking: static and dynamic. In static linking, the linker copies the entire code and data of all required libraries into the final executable file, making it self-contained and independent of any external dependencies. In dynamic linking, the program links to shared libraries at runtime, allowing multiple programs to share the same code and data in memory. The linking process also includes other tasks such as assigning addresses to symbols, performing relocation of code and data, and generating debugging information.
  - Symbol
    - a symbol is a unique identifier that is used to represent a variable, function, or other program entity. Symbols are used by the compiler and linker to resolve references between different parts of a program, such as functions calling other functions or variables being used in multiple files. When a program is compiled, each symbol is assigned an address in memory that represents its location within the program. The linker uses these addresses to link together the different object files that make up the program, so that the program can be executed as a single entity. Symbols can be either global or local. Global symbols are visible to the entire program, and can be accessed from any file or module. Local symbols, on the other hand, are only visible within the file or module in which they are defined, and cannot be accessed from outside that scope. Symbols can also be defined or declared. A symbol is defined when its value or address is explicitly specified in the code, such as when a variable is assigned a value. A symbol is declared when it is referenced in the code but its value or address is not specified, such as when a function is called. Symbols are an important concept in programming, especially in compiled languages like C++ where they are used extensively during the compilation and linking process.

## Libraries

- A library is a collection of precompiled code that can be used by a program to perform certain tasks. Libraries typically contain functions, data structures, and other resources that can be used to simplify programming tasks and reduce the amount of code that needs to be written. Libraries can be either static or dynamic.

  - A static library is a collection of object files that are linked directly into a program at compile time. This means that the library code is included in the final executable file, making it self-contained and not requiring any external dependencies. However, this can also result in larger executable files, as the entire library code is included even if only a small portion of it is used.

  - A dynamic library, on the other hand, is a separate binary file that is loaded into memory at runtime when the program starts. This allows the library to be shared among multiple programs, reducing the overall memory usage of the system. Dynamic libraries can also be updated independently of the programs that use them, making it easier to fix bugs or add new features without having to recompile the entire program
