# cpp_demo

## MyModule

MyModule is a C++ Project, that includes

- Module that provides a set of functions and a class for performing specific operations
- a **Google Test** suite to verify the correctness of the module's functionality
- **Boost** library for MPI functions
- a **CI/CD** pipeline with various stages
- A **static** and **shared** lib in `./libs`. POC - Not very versatile though :)

### Build Instructions

To build the project, follow these steps:

1. Ensure that you have a C++ compiler (preferably g++) installed on your system.
2. Clone the project repository to your local machine.
3. Navigate to the project directory.
4. Open a terminal and run the following command to build the module:
   ```
   make mymodule
   ```
5. (Optional) To build and run the test suite, use the following command:
   ```
   make ENABLE_TESTS=1 mymodule_test
   ```
   This will compile the test code and generate the executable file `mymodule_test`.
6. Execute the module or the test suite:
   - To run the module, use the following command:
   ```
   ./mymodule
   ```
   - To run the test suite, use the following command:
   ```
   ./mymodule_test
   ```
7. Clean the object and executable files with:
   ```
   make clean
   ```

### Features

- `myFunction1(int a, int b)`: A function that takes two integers and returns their sum.
- `mymodule::MyClass`: A class with a constructor that takes three integers (`a, b, c`) and a
  - member function `myFunction2(int a, int b, int c)` that performs a mathematical operation on the inputs.
- `myLambda(int a, int b)`: A lambda function that multiplies two integers and returns the result.

## Dependencies

The project has the following dependencies:

- C++17 compatible compiler (e.g., g++)
- Google Test framework (required only for running the test suite)

### License

This project is licensed under the MIT License. Feel free to use and modify the code according to your needs.
For further details and examples, refer to the source code files.
Enjoy using MyModule!
