#include <iostream>
#include <memory>

class MyClass {
   public:
    MyClass(int data, float f, int i) : mData(data), mFloat(f), mPublicInt(i) {
        std::cout << "MyClass constructor called with data " << mData << " " << mFloat << " " << mPublicInt << std::endl;
    }

    ~MyClass() {
        std::cout << "MyClass destructor called with data " << mData << std::endl;
    }

    void setData(int data) {
        mData = data;
    }

    int getData() const {
        return mData;
    }
    int mPublicInt;

   private:
    int mData;
    float mFloat;
};

void doSomething(std::unique_ptr<MyClass>& ptr) {
    std::cout << "doSomething called with data " << ptr->getData() << std::endl;
    ptr->setData(42);
}

int main() {
    std::unique_ptr<MyClass> ptr(new MyClass(10, 2.0, 5));
    std::cout << "Main called with data " << ptr->getData() << std::endl;
    std::cout << "Main called with mPublicInt " << ptr->mPublicInt << std::endl;
    doSomething(ptr);
    std::cout << "Main called with data " << ptr->getData() << std::endl;
    return 0;
}
