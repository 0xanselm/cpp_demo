#include <iostream>
#include <memory>
#include <string>

class Person {
   public:
    Person(const std::string& name) : m_name(name) {}
    virtual ~Person() {}
    virtual void introduce() const {
        std::cout << "My name is " << m_name << ".\n";
    }

   protected:
    std::string m_name;
};

class Student : public Person {
   public:
    Student(const std::string& name, int id) : Person(name), m_id(id) {}
    void introduce() const override {
        std::cout << "My name is " << Person::m_name << ", and my id is " << m_id << ".\n";
    }

   private:
    int m_id;
};

int main() {
    Person person("Alice");
    Student student("Bob", 123456);
    person.introduce();
    student.introduce();
    return 0;
}